# Controle de Pedidos

## Descrição

Repositório que contem a camada backend do Projeto de Controle de Pedidos Possui os módulos domain, repository e webservice

## Sub-módulos

* domain (Módulo que contem as entidades (objetos de dominio) da aplicação Controle de Pedidos)
* repository (Módulo que contém os repositorios da aplicacao Controle de Pedidos)
* webservice (Módulo que contém os webservices da aplicação Controle de Pedidos)

## Tecnologias e frameworks

* Gradle 4.10.2
* Spring Boot 2.0.5 (Spring Data JPA e REST)

## Como executar

### Execução local através do Gradle

```console
foo@bar:~$ gradle bootRun
```

### Acesso através do Heroku

https://controlepedidosbackend.herokuapp.com/

### URIs

/clientes
/produtos
/pedidos

## Equipe

* Heverson Silva Vasconcelos <heverson.vasconcelos@gmail.com>
* Luis Alberto de Quadros <luislaq@gmail.com>
* Diogo Alexandro Pereira <diogoalexp@gmail.com>
