package br.ufpr.ees.controlepedidos.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "ITEM_PEDIDO")
public class ItemPedido extends AbstractEntity {

    private static final long serialVersionUID = 1L;

    @Column(name = "quantidade")
    private Integer quantidade;

    /**
     * Pedido
     * <p>
     * Referencia: Necessario o uso da anotacao JsonIgnore para evitar a recursao
     * infinita da relacao ManyToOne bidirecional entre Pedido e ItemPedido
     * https://www.baeldung.com/jackson-bidirectional-relationships-and-infinite-recursion
     */
    @ManyToOne
    @JoinColumn(name = "pedido_fk")
    @JsonIgnore
    private Pedido pedido;

    @OneToOne
    @JoinColumn(name = "produto_fk")
    private Produto produto;

    public ItemPedido() {
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public Pedido getPedido() {
        return pedido;
    }

    public void setPedido(Pedido pedido) {
        this.pedido = pedido;
    }

    public Produto getProduto() {
        return produto;
    }

    public void setProduto(Produto produto) {
        this.produto = produto;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ItemPedido)) return false;
        if (!super.equals(o)) return false;

        ItemPedido that = (ItemPedido) o;

        if (pedido != null ? !pedido.equals(that.pedido) : that.pedido != null) return false;
        return produto != null ? produto.equals(that.produto) : that.produto == null;
    }
}
