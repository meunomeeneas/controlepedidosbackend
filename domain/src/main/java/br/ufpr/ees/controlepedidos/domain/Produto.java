package br.ufpr.ees.controlepedidos.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "PRODUTO")
public class Produto extends AbstractEntity {

    private static final long serialVersionUID = 1L;

    @Column(name = "descricao", nullable = false, unique = true, length = 60)
    private String descricao;

    public Produto() {
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    @Override
    public String toString() {
        return this.getDescricao();
    }

}
