package br.ufpr.ees.controlepedidos.domain;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name = "PEDIDO")
public class Pedido extends AbstractEntity {

	private static final long serialVersionUID = 1L;

	@Column(name = "data_pedido", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	@JsonFormat(pattern = "dd-MM-yyyy HH:mm:ss")
	private Date dataPedido;

	@ManyToOne
	@JoinColumn(name = "cliente_fk")
	private Cliente cliente;

	/**
	 * Lista contendo os itens do pedido
	 *
	 * Referencia: Relacao OneToMany bidirecional
	 * https://vladmihalcea.com/a-beginners-guide-to-jpa-and-hibernate-cascade-types/
	 *
	 */
	@OneToMany(mappedBy = "pedido", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private List<ItemPedido> itens = new ArrayList<>();

	public Pedido() {
	}

	public Pedido(Date dataPedido, Cliente cliente, List<ItemPedido> itens) {
		super();
		this.dataPedido = dataPedido;
		this.cliente = cliente;
		this.itens = itens;
	}
	
	@PrePersist
	@PreUpdate
	public void updateItensPedido() {
		for(ItemPedido item : itens) {
			item.setPedido(this);
		}
	}

	public Date getDataPedido() {
		return dataPedido;
	}

	public void setDataPedido(Date dataPedido) {
		this.dataPedido = dataPedido;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public List<ItemPedido> getItensPedido() {
		return itens;
	}

	public void setItensPedido(List<ItemPedido> itensPedido) {
		this.itens = itensPedido;
	}

}
