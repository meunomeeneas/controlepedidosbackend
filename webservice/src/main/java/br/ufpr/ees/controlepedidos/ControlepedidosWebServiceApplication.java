package br.ufpr.ees.controlepedidos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EntityScan(basePackages = {"br.ufpr.ees.controlepedidos.domain"})
@EnableJpaRepositories(basePackages = {"br.ufpr.ees.controlepedidos.repository"})
public class ControlepedidosWebServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(ControlepedidosWebServiceApplication.class, args);
	}
}
