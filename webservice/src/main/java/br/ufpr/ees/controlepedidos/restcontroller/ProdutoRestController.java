package br.ufpr.ees.controlepedidos.restcontroller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.ufpr.ees.controlepedidos.domain.Produto;
import br.ufpr.ees.controlepedidos.repository.ProdutoRepository;

@RestController
public class ProdutoRestController {

	@Autowired
	private ProdutoRepository produtoRepository;

	@GetMapping("/produtos")
	public ResponseEntity<List<Produto>> findAll() {
		List<Produto> produtos = produtoRepository.findAll();

		return new ResponseEntity<List<Produto>>(produtos, HttpStatus.OK);
	}

	@GetMapping("/produtos/{id}")
	public ResponseEntity<Produto> findById(@PathVariable Long id) {
		Optional<Produto> produtoOptional = produtoRepository.findById(id);

		if (!produtoOptional.isPresent()) {
			return new ResponseEntity<Produto>(HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<Produto>(produtoOptional.get(), HttpStatus.OK);
	}

	@GetMapping("/produtos/find")
	public ResponseEntity<Produto> findByDescricao(@RequestParam("descricao") String descricao) {
		Produto produto = produtoRepository.findByDescricao(descricao);

		if (produto == null) {
			return new ResponseEntity<Produto>(HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<Produto>(produto, HttpStatus.OK);
	}

	@PostMapping("/produtos")
	public ResponseEntity<Produto> create(@RequestBody Produto produto) {
		return new ResponseEntity<Produto>(produtoRepository.save(produto), HttpStatus.CREATED);
	}

	@PutMapping("/produtos/{id}")
	public ResponseEntity<Produto> update(@RequestBody Produto produto, @PathVariable Long id) {
		Optional<Produto> produtoOptional = produtoRepository.findById(id);

		if (!produtoOptional.isPresent()) {
			return new ResponseEntity<Produto>(HttpStatus.NOT_FOUND);
		}

		produto.setId(id);

		return new ResponseEntity<Produto>(produtoRepository.save(produto), HttpStatus.OK);
	}

	@DeleteMapping("/produtos/{id}")
	public ResponseEntity<Produto> delete(@PathVariable Long id) {
		Optional<Produto> produtoOptional = produtoRepository.findById(id);

		if (!produtoOptional.isPresent()) {
			return new ResponseEntity<Produto>(HttpStatus.NOT_FOUND);
		}

		produtoRepository.deleteById(id);

		return new ResponseEntity<Produto>(produtoOptional.get(), HttpStatus.OK);

	}

}
