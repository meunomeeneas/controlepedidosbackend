package br.ufpr.ees.controlepedidos.restcontroller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import br.ufpr.ees.controlepedidos.domain.Pedido;
import br.ufpr.ees.controlepedidos.repository.PedidoRepository;

@RestController
public class PedidoRestController {

	@Autowired
	private PedidoRepository pedidoRepository;

	/**
	 * Metodo: GET URL: "/pedidos"
	 *
	 * @return Retorna todos os pedidos da base
	 */
	@GetMapping("/pedidos")
	public ResponseEntity<List<Pedido>> findAll() {
		List<Pedido> pedidos = pedidoRepository.findAll();

		return new ResponseEntity<List<Pedido>>(pedidos, HttpStatus.FOUND);
	}

	/**
	 * Metodo: GET URL: "/pedidos/{id}"
	 *
	 * @param id do pedido
	 * @return Retorna um pedido por ID
	 */
	@GetMapping("/pedidos/{id}")
	public ResponseEntity<Pedido> findById(@PathVariable Long id) {
		Optional<Pedido> pedidoOptional = pedidoRepository.findById(id);

		if (!pedidoOptional.isPresent()) {
			return new ResponseEntity<Pedido>(HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<Pedido>(pedidoOptional.get(), HttpStatus.OK);
	}

	/**
	 * Metodo: GET URL: "/pedidos/cliente/{cpf}"
	 *
	 * @param CPF do cliente
	 * @return Retorna a lista de pedidos, dado o CPF do cliente
	 */
	@GetMapping("/pedidos/cliente/{cpf}")
	public ResponseEntity<List<Pedido>> findByCpf(@PathVariable String cpf) {
		List<Pedido> pedidos = pedidoRepository.findByClienteCpf(cpf);

		if (pedidos == null || pedidos.isEmpty()) {
			return new ResponseEntity<List<Pedido>>(HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<List<Pedido>>(pedidos, HttpStatus.OK);
	}

	@GetMapping("/pedidos/produto/{id}")
	public ResponseEntity<List<Pedido>> findByItensProdutoId(@PathVariable Long id) {
		List<Pedido> pedidos = pedidoRepository.findByItensProdutoId(id);

		if (pedidos == null || pedidos.isEmpty()) {
			return new ResponseEntity<List<Pedido>>(HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<List<Pedido>>(pedidos, HttpStatus.OK);
	}

	@PostMapping("/pedidos")
	public ResponseEntity<Pedido> create(@RequestBody Pedido pedido) {
		return new ResponseEntity<Pedido>(pedidoRepository.save(pedido), HttpStatus.CREATED);
	}

}
