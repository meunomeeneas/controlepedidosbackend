package br.ufpr.ees.controlepedidos.restcontroller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import br.ufpr.ees.controlepedidos.domain.Cliente;
import br.ufpr.ees.controlepedidos.repository.ClienteRepository;

@RestController
public class ClienteRestController {

	@Autowired
	private ClienteRepository clienteRepository;

	@GetMapping("/clientes")
	public ResponseEntity<List<Cliente>> findAll() {
		List<Cliente> clientes = clienteRepository.findAll();

		return new ResponseEntity<List<Cliente>>(clientes, HttpStatus.OK);
	}

	@GetMapping("/clientes/{id}")
	public ResponseEntity<Cliente> findById(@PathVariable Long id) {
		Optional<Cliente> clienteOptional = clienteRepository.findById(id);

		if (!clienteOptional.isPresent()) {
			return new ResponseEntity<Cliente>(HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<Cliente>(clienteOptional.get(), HttpStatus.OK);
	}

	@GetMapping("/clientes/cpf/{cpf}")
	public ResponseEntity<Cliente> findByCpf(@PathVariable String cpf) {
		Cliente cliente = clienteRepository.findByCpf(cpf);

		if (cliente == null) {
			return new ResponseEntity<Cliente>(HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<Cliente>(cliente, HttpStatus.OK);
	}

	@PostMapping("/clientes")
	public ResponseEntity<Cliente> create(@RequestBody Cliente cliente) {
		return new ResponseEntity<Cliente>(clienteRepository.save(cliente), HttpStatus.CREATED);
	}

	@PutMapping("/clientes/{id}")
	public ResponseEntity<Cliente> update(@RequestBody Cliente cliente, @PathVariable Long id) {
		Optional<Cliente> clienteOptional = clienteRepository.findById(id);

		if (!clienteOptional.isPresent()) {
			return new ResponseEntity<Cliente>(HttpStatus.NOT_FOUND);
		}

		cliente.setId(id);

		return new ResponseEntity<Cliente>(clienteRepository.save(cliente), HttpStatus.OK);
	}

	@DeleteMapping("/clientes/{id}")
	public ResponseEntity<Cliente> delete(@PathVariable Long id) {
		Optional<Cliente> clienteOptional = clienteRepository.findById(id);

		if (!clienteOptional.isPresent()) {
			return new ResponseEntity<Cliente>(HttpStatus.NOT_FOUND);
		}

		clienteRepository.deleteById(id);

		return new ResponseEntity<Cliente>(clienteOptional.get(), HttpStatus.OK);
	}
}
