package br.ufpr.ees.controlepedidos.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.ufpr.ees.controlepedidos.domain.Cliente;

public interface ClienteRepository extends JpaRepository<Cliente, Long>{
	
	public Cliente findByCpf(String cpf);
		
}
