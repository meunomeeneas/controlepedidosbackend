package br.ufpr.ees.controlepedidos.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.ufpr.ees.controlepedidos.domain.Produto;

public interface ProdutoRepository extends JpaRepository<Produto, Long> {

  public Produto findByDescricao(String descricao);
}
