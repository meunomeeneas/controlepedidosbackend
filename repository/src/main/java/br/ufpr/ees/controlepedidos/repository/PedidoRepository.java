package br.ufpr.ees.controlepedidos.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.ufpr.ees.controlepedidos.domain.Pedido;

public interface PedidoRepository extends JpaRepository<Pedido, Long> {

	public List<Pedido> findByClienteCpf(String cpf);

	public List<Pedido> findByItensProdutoId(Long id);
}
